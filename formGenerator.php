<?php

if (empty($_POST['host'])) {
    echo "host não informado";
    exit;
}
if (empty($_POST['banco'])) {
    echo "banco não informado";
    exit;
}
if (empty($_POST['user'])) {
    echo "user não informado";
    exit;
}
if (empty($_POST['tabela'])) {
    echo "tabela não informado";
    exit;
}

$pdo = new \PDO("mysql:host={$_POST['host']};dbname={$_POST['banco']}", $_POST['user'], !empty($_POST['senha']) ? $_POST['senha'] : '');

$tabela = $_POST['tabela'];
$inputidden = $_POST['inputidden'];
$view = str_replace("_", "", $tabela);

$colunas_array = array();
$stmt = $pdo->prepare("SHOW COLUMNS FROM $tabela ");
if ($stmt->execute()) {
    $row = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    foreach ($row as $value) {
        array_push($colunas_array, $value);
    }
}

/*
 * form
 */
//nome da class
$conteudoForm = '<div class="row">
<div class="col-lg-12">
    <div class="ibox ">
        <form role="form" method="post" id="form-' . strtolower($view) . '" autocomplete="off">';

if (!empty($inputidden)) {
    $idden = explode(",", $inputidden);
    foreach ($idden as $input) {
        $conteudoForm .= '<input type="hidden" id="' . $input . '" name="' . $input . '" value="<?php echo!empty($this->' . strtolower($view) . '["' . $input . '"]) ? $this->' . strtolower($view) . '["' . $input . '"] : ""; ?>" />';
    }
}

foreach ($colunas_array as $c) {

    $type = explode("(", $c['Type']);

    switch (current($type)) {
        case 'bigint':
            if ($c['Field'] != "id_" . strtolower($tabela)) {
                $viewjoin = current(explode("id_", str_replace("_", "", $c['Field'])));
                $conteudoForm .= '<div class="col-md-3 form-group">
                <label>' . ucfirst(str_replace("id_", "", $c['Field'])) . '</label>
                <select class="form-control ctrl_select" name="' . $c['Field'] . '">
                    <option></option>
                    <?php
                    if (!empty($this->' . $viewjoin . ')) {
                        foreach ($this->' . $viewjoin . ' as $e) {
                    ?>
                            <option value="<?php echo $e["' . $c['Field'] . '"] ?>" <?php echo !empty($this->' . strtolower($view) . '["' . $c['Field'] . '"]) && $this->' . strtolower($view) . '["' . $c['Field'] . '"] == $e["' . $c['Field'] . '"] ? "selected" : ""; ?>>
                                <?php echo $e["no_' . str_replace("id_", "", $c['Field']) . '"]; ?>
                            </option>
                    <?php
                        }
                    } //fim do if
                    ?>
                </select>
            </div>';
            }
            break;
        case 'int':
            if ($c['Field'] != "id_" . strtolower($tabela)) {
                $viewjoin = current(explode("id_", str_replace("_", "", $c['Field'])));
                $conteudoForm .= '<div class="col-md-3 form-group">
                <label>' . ucfirst(str_replace("id_", "", $c['Field'])) . '</label>
                <select class="form-control ctrl_select" name="' . $c['Field'] . '">
                    <option></option>
                    <?php
                    if (!empty($this->' . $viewjoin . ')) {
                        foreach ($this->' . $viewjoin . ' as $e) {
                    ?>
                            <option value="<?php echo $e["' . $c['Field'] . '"] ?>" <?php echo !empty($this->' . strtolower($view) . '["' . $c['Field'] . '"]) && $this->' . strtolower($view) . '["' . $c['Field'] . '"] == $e["' . $c['Field'] . '"] ? "selected" : ""; ?>>
                                <?php echo $e["no_' . str_replace("id_", "", $c['Field']) . '"]; ?>
                            </option>
                    <?php
                        }
                    } //fim do if
                    ?>
                </select>
            </div>';
            }
            break;
        case 'varchar':
            $conteudoForm .= '<div class="col-md-4 form-group">
                                <label>' . $c['Field'] . '</label>
                                <input type="text" name="' . $c['Field'] . '" class="form-control" value="<?php echo !empty($this->' . strtolower($view) . '["nm_cpf_cnpj"]) ? $this->' . strtolower($view) . '["nm_cpf_cnpj"] : "" ?>">
                              </div>';
            break;
        case 'decimal':
            $conteudoForm .= '<div class="col-md-4 form-group">
                                <label>' . $c['Field'] . '</label>
                                <input type="text" name="' . $c['Field'] . '" class="form-control" value="<?php echo !empty($this->' . strtolower($view) . '["nm_cpf_cnpj"]) ? $this->' . strtolower($view) . '["nm_cpf_cnpj"] : "" ?>">
                              </div>';
            break;
        case 'date':
            $conteudoForm .= '<div class="col-md-4 form-group datarange">
            <label>' . $c['Field'] . '</label>
            <div class="input-group date">
                <input type="text" class="form-control" name="' . $c['Field'] . '" 
                       value="<?php echo!empty($this->' . strtolower($view) . '[' . $c['Field'] . ']) ? date("d/m/Y", strtotime($this->' . strtolower($view) . '[' . $c['Field'] . '])) : "" ?>">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>';
            break;
        case 'text':
            $conteudoForm .= '<div class="col-md-12 form-group">
                                    <label>' . $c['Field'] . '</label>
                                    <input type="text" name="' . $c['Field'] . '" class="form-control" value="<?php echo !empty($this->' . strtolower($view) . '["nm_cpf_cnpj"]) ? $this->' . strtolower($view) . '["nm_cpf_cnpj"] : "" ?>">
                                  </div>';
            break;
        case 'bigtext':
            $conteudoForm .= '<div class="col-md-12 form-group">
                                        <label>' . $c['Field'] . '</label>
                                        <input type="text" name="' . $c['Field'] . '" class="form-control" value="<?php echo !empty($this->' . strtolower($view) . '["nm_cpf_cnpj"]) ? $this->' . strtolower($view) . '["nm_cpf_cnpj"] : "" ?>">
                                      </div>';
            break;

        default:
            $conteudoForm .= '';
            break;
    }
}
$conteudoForm .= '</form>
</div>
</div>
</div>';

$arquivo_name = "form.php";

if (!empty($conteudoForm)) {
    //$nome = 'Response.php';
    $arquivo = fopen($arquivo_name, 'w');
    fwrite($arquivo, $conteudoForm);
    fclose($arquivo);

    $file = $arquivo_name;

    header("Content-Length: " . filesize($file));
    // informa o tamanho do file ao navegador
    header("Content-Disposition: attachment; filename=" . basename($file));
    // informa ao navegador que é tipo anexo e faz abrir a janela de download,
    //tambem informa o nome do file
    readfile($file); // lê o file
    unlink("./$arquivo_name");
    exit; // aborta pós-ações
} else {
    echo "Escolha ao menos uma opção";
    exit;
}
