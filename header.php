<!DOCTYPE html>
<html lang="en">

<head>
    <title>Gerador de Classes</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <style type="text/css">
    .container{
        margin: 100px;
    }
        .was-validated {
            margin-bottom: 150px;
        }
    </style>
</head>

<body>
<nav class="navbar navbar-expand-sm bg-light">
  <!-- Links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="classe.php">Classe</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Index Listagem</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Index Filtros</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="form.php">Form</a>
    </li>
  </ul>
</nav>