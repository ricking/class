<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "matina"; // banco de origem
$dbnameTo = "gam_matina"; // banco de destino

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
$connTo = new mysqli($servername, $username, $password, $dbnameTo); // conexão de destino
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
if ($connTo->connect_error) {
    die("Connection failed: " . $connTo->connect_error);
}

$sql = "SELECT p.*, l.LOGR as LOGRADOURO, b.NOME as BAIRRO FROM paciente p
LEFT JOIN logradouro l ON p.COD_RUA = l.COD_RUA
LEFT JOIN bairro b ON b.COD_BAIRRO = p.COD_BAIRRO;";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo "<table><tr>
    <th>ID</th>
    <th>Name</th>
    <th>Endereco</th>
    <th>Status Migração</th>
    </tr>";

    while ($row = $result->fetch_assoc()) {
        echo "<tr><td>" . $row["COD_PACIENTE"] . "</td><td>" . ($row["NOME_PAC"] ?? 'Desconhecido') . "</td>";

        // Verifica se o paciente já existe
        $id_pessoa = checkIfPacienteExists($connTo, $row["CPF_PAC"] ?? '', $row["CARSUS"] ?? '');
        if ($id_pessoa) {
            echo "<td>ID: $id_pessoa</td>";
            echo "</tr>";
            continue;
        }

        // Inserir endereço e obter ID
        $id_endereco = insertEndereco($connTo, $row);

        // Inserir estado civil e obter ID
        $id_estado_civil = insertOrUpdateEstadoCivil($connTo, $row["ESTCIVIL"] ?? '');

        // Inserir profissão e obter ID
        $id_profissao = insertOrUpdateProfissao($connTo, $row["PROFISSAO"] ?? '');

        // Determina o sexo
        $dc_sexo = determineSexo($row["SEXO"] ?? null);

        // Inserir pessoa
        $id_pessoa = insertPessoa($connTo, $row, $id_estado_civil, $id_profissao, $id_endereco, $dc_sexo);
        if ($id_pessoa) {
            echo "<td>PESSOA CADASTRADA COM SUCESSO, ID: $id_pessoa</td>";
        } else {
            echo "<td>ERRO: " . $connTo->error . "</td>";
        }
        echo "</tr>";
    }
    echo "</table>";
} else {
    echo "0 results";
}

$conn->close();
$connTo->close();

function checkIfPacienteExists($conn, $cpf, $sus)
{
    // Verifica se o CPF é fornecido e não está vazio.
    if (!empty($cpf) && $cpf > 10000) {
        $query = $conn->prepare("SELECT id_pessoa FROM pessoa WHERE nm_cpf = ?");
        $query->bind_param("s", $cpf);
        $query->execute();
        $result = $query->get_result();
        $id_pessoa = $result->fetch_assoc()['id_pessoa'] ?? false;
        if ($id_pessoa) {
            return $id_pessoa;  // Retorna o ID se encontrado pelo CPF.
        }
    }

    // Verifica se o número do SUS é fornecido e não está vazio.
    if (!empty($sus) && $sus > 10000) {
        $query = $conn->prepare("SELECT id_pessoa FROM pessoa WHERE nm_sus = ?");
        $query->bind_param("s", $sus);
        $query->execute();
        $result = $query->get_result();
        $id_pessoa = $result->fetch_assoc()['id_pessoa'] ?? false;
        if ($id_pessoa) {
            return $id_pessoa;  // Retorna o ID se encontrado pelo número do SUS.
        }
    }

    return false;  // Retorna false se nenhum ID foi encontrado ou se ambos os parâmetros estão vazios.
}


function insertEndereco($conn, $row)
{
    if (!empty($row["CEP"])) {
        $query = $conn->prepare("INSERT INTO endereco (nm_cep, dc_logradouro, dc_complemento, nm_numero, dc_bairro, dc_cidade, dc_uf, dc_uf_descricao)
        VALUES (?, ?, ?, ?, ?, '', '', '')");
        $query->bind_param("sssss", $row["CEP"], $row["LOGRADOURO"], $row["COMPL"], $row["NUMERO"], $row["BAIRRO"]);
        $query->execute();
        return $conn->insert_id;
    }else{
        return null;
    }

}

function insertOrUpdateEstadoCivil($conn, $estadoCivil)
{
    if (empty($estadoCivil)) {
        return null;
    }
    $query = $conn->prepare("INSERT INTO estado_civil (no_estado_civil) VALUES (?) ON DUPLICATE KEY UPDATE no_estado_civil = VALUES(no_estado_civil)");
    $query->bind_param("s", $estadoCivil);
    $query->execute();
    return $conn->insert_id ?: getEstadoCivilId($conn, $estadoCivil);
}

function getEstadoCivilId($conn, $estadoCivil)
{
    $query = $conn->prepare("SELECT id_estado_civil FROM estado_civil WHERE no_estado_civil = ?");
    $query->bind_param("s", $estadoCivil);
    $query->execute();
    $result = $query->get_result();
    return $result->fetch_assoc()['id_estado_civil'] ?? null;
}

function insertOrUpdateProfissao($conn, $profissao)
{
    if (empty($profissao)) {
        return null;
    }
    $query = $conn->prepare("INSERT INTO profissao (no_profissao) VALUES (?) ON DUPLICATE KEY UPDATE no_profissao = VALUES(no_profissao)");
    $query->bind_param("s", $profissao);
    $query->execute();
    return $conn->insert_id ?: getProfissaoId($conn, $profissao);
}

function getProfissaoId($conn, $profissao)
{
    $query = $conn->prepare("SELECT id_profissao FROM profissao WHERE no_profissao = ?");
    $query->bind_param("s", $profissao);
    $query->execute();
    $result = $query->get_result();
    return $result->fetch_assoc()['id_profissao'] ?? null;
}

function determineSexo($sexoCode)
{
    switch ($sexoCode) {
        case 1:return 'M';
        case 3:return 'F';
        default:return null;
    }
}

function insertPessoa($conn, $row, $id_estado_civil, $id_profissao, $id_endereco, $dc_sexo)
{
    $query = $conn->prepare("INSERT INTO pessoa (id_pessoa_tipo, no_nome_completo, nm_sus, id_estado_civil, id_profissao, nm_telefone, id_endereco, nm_cpf, no_nome_responsavel, dc_sexo, dt_nascimento, no_nome_mae, no_nome_pai, dc_uf_nascimento, id_codigo_externo, dc_alergia, dc_medicacao_controlada, dc_medicacao_uso_continuo, flag_diabetico, flag_hipertenso)
                             VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

    $id_pessoa_tipo = 8; // Supondo que seja um valor fixo
    $dc_uf_nascimento = $row['UF_NATURAL'] ?? null; // exemplo de como tratar possíveis NULLs
    $id_codigo_externo = $row['COD_PACIENTE'] ?? null;

    // Adapte para garantir que todos os campos estejam corretamente tipados
    $query->bind_param("issiisssssssssssssss",
        $id_pessoa_tipo,
        $row["NOME_PAC"],
        $row["CARSUS"],
        $id_estado_civil,
        $id_profissao,
        $row["TEL_RESID"],
        $id_endereco,
        $row["CPF_PAC"],
        $row["NOME_RESP"],
        $dc_sexo,
        $row["DT_NASC"],
        $row["NOME_MAE"],
        $row["NOME_PAI"],
        $dc_uf_nascimento,
        $id_codigo_externo,
        $row["PAC_ALERGIA_DESCR"],
        $row["PAC_MEDICA_CONTROLADO"],
        $row["PAC_MEDICA_USOCONTINUO"],
        $row["PAC_DIABETICO"],
        $row["PAC_HIPERTENSO"]
    );

    $query->execute();
    return $conn->insert_id;
}
