<?php

//use PDO;
require_once 'conexao.php';

$tabela = $_POST['tabela'];
$theme = $_POST['thema'];
$restricted = $_POST['restricted'];

$controller = ucfirst(str_replace("_","",$tabela));
$view = str_replace("_","",$tabela);

$prefixo = substr($tabela, 0, 3);

$colunas = array();
$stmt = $pdo->prepare("SHOW COLUMNS FROM $tabela ");
if ($stmt->execute()) {
    $row = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    foreach ($row as $value) {
        array_push($colunas, $value['Field']);
    }
}
/*
 * Controller
 */
//nome da class
$conteudo = '<?php
namespace App\controllers;
use Framework\core\Controller;
use App\models\/' . $controller . 'Model;

class ' . $controller . 'Controller extends Controller {';

//contruct
$conteudo .= '
    public function __construct() {
        parent::__construct();
        $this->view->setActive("' . $view . '");';

$conteudo .= !$restricted ? '$this->restricted();' : '';
$conteudo .= '}';

//index
$conteudo .= 'public function index() {
        $this->view->setTheme("' . $theme . '");

        $' . $prefixo . ' = new ' . $controller . 'Model; 
        $' .$view . 's = $' . $prefixo . '->get' . $controller . 's();
        $this->view->setRegistros(sizeof($' .$view . 's), "total");
        $this->view->setRegistros($' .$view . 's, "' .$view . 's");

        $this->view->setOthercss(array(
            "plugins/bootstrap-sweet-alerts/sweet-alert",
            "css/plugins/jasny/jasny-bootstrap.min",
            "css/plugins/select2/select2.min",
            "css/plugins/dataTables/datatables.min",
            "css/plugins/iCheck/custom",
            "css/plugins/cropper/cropper.min",
            "css/plugins/datapicker/datepicker3"));

        $this->view->setOtherjs(array(
            "plugins/bootstrap-sweet-alerts/sweet-alert.min",
            "js/plugins/jasny/jasny-bootstrap.min",
            "js/plugins/select2/select2.full.min",
            "js/plugins/dataTables/datatables.min",
            "js/plugins/dataTables/dataTables.bootstrap4.min",
            "js/plugins/fullcalendar/moment.min",
            "js/plugins/iCheck/icheck.min",
            "js/plugins/cropper/cropper.min",
            "js/plugins/datapicker/bootstrap-datepicker"));
        $this->view->setCSS(array("shared/select2"));
        $this->view->setJS(array("index"));
        $this->view->render("index");
    }';

//method form
$conteudo .= 'function form($id = 0) {

        $this->view->setTheme("' . $theme . '");
        if ($id) {
            $' . $prefixo . ' = new ' . $controller . 'Model;
            $' . $prefixo . '->set' . ucfirst($colunas[0]) . '($id);
            $' .$view . ' = $' . $prefixo . '->get' . $controller . '();
            $this->view->setRegistros($' .$view . ', "' .$view . '");
        }
        $this->view->setOthercss(array(
        "plugins/bootstrap-sweet-alerts/sweet-alert",
        "css/plugins/jasny/jasny-bootstrap.min",
            "css/plugins/datapicker/datepicker3", 
            "css/plugins/select2/select2.min",
            "css/plugins/dataTables/datatables.min",
            "css/plugins/iCheck/custom"));

        $this->view->setOtherjs(array(
        "plugins/bootstrap-sweet-alerts/sweet-alert.min",
        "js/plugins/datapicker/bootstrap-datepicker",
            "js/plugins/jasny/jasny-bootstrap.min",
            "js/plugins/select2/select2.full.min",
            "js/plugins/dataTables/datatables.min",
            "js/plugins/dataTables/dataTables.bootstrap4.min",
            "js/plugins/iCheck/icheck.min"));
        $this->view->setCSS(array("shared/select2"));
        $this->view->setJS(array("form"));
        $this->view->render("form");
    }';

//method set
$conteudo .= 'function set() {
    $' . $prefixo . ' = new ' . $controller . 'Model;';
foreach ($colunas as $c) {
    $conteudo .= '$' . $prefixo . '->set' . ucfirst($c) . '($this->getPostParam("' . $c . '"));';
}
$conteudo .= '$id = $' . $prefixo . '->set();
        if ($id) {
            echo json_encode(array("success" => true, "message" => "Dados cadastrados com sucesso!", "id" => $id));
            exit;
        } else {
            echo json_encode(array("error" => true, "message" => "Ocorreu um erro, por favor tente novamente!"));
            exit;
        }
    }';

//delete
$conteudo .= 'function del($id) {
        if ($this->filterInt($id)) {
            $' . $prefixo . ' = new ' . $controller . 'Model;
            $' . $prefixo . '->set' . ucfirst($colunas[0]) . '($id);
            if ($' . $prefixo . '->delete()) {
                echo json_encode(array("success" => true, "message" => "O cadastro foi excluído com sucesso!"));
            }
        }
    }
    
}';
$nome = $controller . 'Controller.php';

echo $conteudo;

// if(!file_exists($nome)){
//     $arquivo = fopen($nome, 'w');
//     fwrite($arquivo, $conteudo);
//     fclose($arquivo);
// }


