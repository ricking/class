<?php

//use PDO;
require_once './conexao.php';

$tabela = $_POST['tabela'];

$prefixo = substr($tabela, 0, 3);

$model = ucfirst(str_replace("_", "", $tabela));

$colunas = array();
$stmt = $pdo->prepare("SHOW COLUMNS FROM $tabela ");
if ($stmt->execute()) {
    $row = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    foreach ($row as $value) {
        array_push($colunas, $value['Field']);
    }
}

/*
 * Model
 */

//nome da class
$conteudo = '<?php
namespace App\models;
use Framework\core\Model;
class ' . $model . 'Model extends Model {';

$conteudo .= 'private ';
foreach ($colunas as $c) {
    $conteudo .= '$' . $c;
    if ($c != end($colunas)) {
        $conteudo .= ',';
    }
}
$conteudo .= ';';

$conteudo .= 'function __construct() {
        parent::__construct();
    }';

//method getAll
$conteudo .= 'function get' . $model . 's() {
        $sql = "SELECT * FROM ' . $tabela . ' ORDER BY ' . $colunas[0] . ' ASC";
        $res = $this->query($sql);
        return $res->fetchAll();
    }';

//method get
$conteudo .= 'function get' . $model . '() {
        $sql = "SELECT * FROM ' . $tabela . ' WHERE ' . $colunas[0] . ' = ?";
        $params = array($this->get' . ucfirst($colunas[0]) . '());
        $res = $this->query($sql, $params);
        return $res->fetch();
    }';

//method set
$conteudo .= 'function set() {
        if ($this->get' . ucfirst($colunas[0]) . '()) {
            $sql = "UPDATE ' . $tabela . ' SET ';
foreach ($colunas as $c) {
    $conteudo .= $c . ' = ?';
    if ($c != end($colunas)) {
        $conteudo .= ',';
    }
}

$conteudo .= ' WHERE ' . $colunas[0] . ' = ?;";';
$conteudo .= ' $params = array(';
foreach ($colunas as $c) {
    $conteudo .= '$this->get' . ucfirst($c) . '(),';
}
$conteudo .= '$this->get' . ucfirst($colunas[0]) . '()';
$conteudo .= ');';
$conteudo .= '$res = $this->query($sql, $params);';
$conteudo .= '$res->rowCount();';
$conteudo .= 'return $this->get' . ucfirst($colunas[0]) . '();';
$conteudo .= '} else {';
$conteudo .= '$sql = "INSERT INTO ' . $tabela . ' (';

foreach ($colunas as $c) {
    $conteudo .= $c;
    if ($c != end($colunas)) {
        $conteudo .= ',';
    }
}

$conteudo .= ') VALUES (';

foreach ($colunas as $c) {
    $conteudo .= '?';
    if ($c != end($colunas)) {
        $conteudo .= ',';
    }
}

$conteudo .= ')";';
$conteudo .= ' $params = array(';
foreach ($colunas as $c) {
    $conteudo .= '$this->get' . ucfirst($c) . '()';
    if ($c != end($colunas)) {
        $conteudo .= ',';
    }
}
$conteudo .= ');';
$conteudo .= '$this->query($sql, $params);';
$conteudo .= 'return $this->lastInsertId;';
$conteudo .= ' } }';



//delete
$conteudo .= 'function delete() {
        $sql = "DELETE FROM ' . $tabela . ' WHERE ' . $colunas[0] . ' = ?;";
        $params = array($this->get' . ucfirst($colunas[0]) . '());
        $res = $this->query($sql, $params);
        return $res->rowCount();
    }';

foreach ($colunas as $c) {
    $conteudo .= 'function get' . ucfirst($c) . '() {
            return $this->' . $c . ';
        }';
}

foreach ($colunas as $c) {
    $conteudo .= 'function set' . ucfirst($c) . '($' . $c . ') {
            $this->' . $c . ' = $' . $c . ';
        }';
}


$conteudo .= '}';

$nome = $model . 'Model.php';
$arquivo = fopen($nome, 'w');
fwrite($arquivo, $conteudo);
fclose($arquivo);

$file = $nome;

header("Content-Length: " . filesize($file));
// informa o tamanho do file ao navegador
header("Content-Disposition: attachment; filename=" . basename($file));
// informa ao navegador que é tipo anexo e faz abrir a janela de download,
//tambem informa o nome do file
readfile($file); // lê o file
exit; // aborta pós-ações
