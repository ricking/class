<?php include "header.php"; ?>

<div class="container">
    <div class="text-center">
        <h2>Gerador de Classes</h2>
        <p>Preencha os campos com as informações corretamente</p>
        <form action="classGenerator.php" method="POST" class="was-validated">
            <div class="row">
                <div class="form-group col-md-3">
                    <label for="host">DB_HOST:</label>
                    <input type="text" class="form-control" id="host" placeholder="localhost" name="host" required value="localhost">
                    <div class="valid-feedback">OK</div>
                    <div class="invalid-feedback">Campo obrigatório.</div>
                </div>
                <div class="form-group col-md-3">
                    <label for="banco">DB_NAME:</label>
                    <input type="text" class="form-control" id="banco" placeholder="nome do banco" name="banco" required>
                    <div class="valid-feedback">OK</div>
                    <div class="invalid-feedback">Campo obrigatório.</div>
                </div>
                <div class="form-group col-md-3">
                    <label for="user">DB_USER:</label>
                    <input type="text" class="form-control" id="user" placeholder="usuário" name="user" required value="root">
                    <div class="valid-feedback">OK</div>
                    <div class="invalid-feedback">Campo obrigatório.</div>
                </div>
                <div class="form-group col-md-3">
                    <label for="senha">DB_PASS:</label>
                    <input type="text" class="form-control" id="senha" placeholder="Deixar vazio se não tiver senha" name="senha">
                </div>
                <div class="form-group col-md-3">
                    <label for="tabela">TABELA:</label>
                    <input type="text" class="form-control" id="tabela" placeholder="nome da tabela" name="tabela" required>
                    <div class="valid-feedback">OK</div>
                    <div class="invalid-feedback">Campo obrigatório.</div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <h3>Controller</h3>
                </div>
                <div class="form-group col-md-3">
                    <label for="thema">TEMA:</label>
                    <input type="text" class="form-control" id="thema" placeholder="default" name="thema">
                </div>
                <div class="form-group col-md-9">
                    <label for="bloco_style">BLOCO CSS AND JS:</label>
                    <textarea name="bloco_style" class="form-control" rows="5"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <h3>Index</h3>
                </div>
                <div class="form-group col-md-3">
                    <label for="dashboard">ROTA DASHBOARD:</label>
                    <input type="text" class="form-control" id="dashboard" placeholder="dashboard" name="dashboard">
                </div>
                <div class="form-group col-md-9">
                    <label for="selects">SELECTS NA VIEW:</label>
                    <input type="text" class="form-control" id="selects" placeholder="tabela,tabela..." name="selects">
                </div>
                <div class="form-group col-md-6">
                    <label for="filtros">FILTROS:</label>
                    <input type="text" class="form-control" id="filtros" placeholder="tabela,coluna,tipo;tabela,campo,tipo..." name="filtros">
                </div>
                <div class="form-group col-md-6">
                    <label for="colunasPesquisa">COLUNAS DA TABELA DE PESQUISA:</label>
                    <input type="text" class="form-control" id="colunasPesquisa" placeholder="coluna,coluna,coluna..." name="colunasPesquisa">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <h3>Form</h3>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputidden">INPUT IDDEN:</label>
                    <input type="text" class="form-control" id="inputidden" placeholder="id_idden,id_idden,id_idden..." name="inputidden">
                </div>
            </div>
            <div class="form-group form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="restricted" value="1"> O Controller é restrito?
                </label>
            </div>
            <div class="form-group form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="checkbyname" value="1"> Checke novo cadastro por nome?
                </label>
            </div>
            <div class="form-group form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="model"> Criar Model.
                </label>
            </div>
            <div class="form-group form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="controller"> Criar Controller.
                </label>
            </div>
            <div class="form-group form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="index"> Criar Index.
                </label>
            </div>
            <div class="form-group form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="form"> Criar Form.
                </label>
            </div>
            <div class="form-group form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="interface"> Criar Interface TypeScript.
                </label>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

</div>

</body>

</html>