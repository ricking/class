<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (empty($_POST['host'])) {
    echo "host não informado";
    exit;
}
if (empty($_POST['banco'])) {
    echo "banco não informado";
    exit;
}
if (empty($_POST['user'])) {
    echo "user não informado";
    exit;
}
if (empty($_POST['tabela'])) {
    echo "tabela não informado";
    exit;
}

$bloco_style = "";

if (!empty($_POST['bloco_style'])) {
    $bloco_style = $_POST['bloco_style'];
}

$pdo = new \PDO("mysql:host={$_POST['host']};dbname={$_POST['banco']}", $_POST['user'], !empty($_POST['senha']) ? $_POST['senha'] : '');

$tabela = $_POST['tabela'];
$theme = $_POST['thema'];
$selects = $_POST['selects'];
$filtros = $_POST['filtros'];
$joins = !empty($_POST['joins']) ? $_POST['joins'] : null;
$colunasSelect = !empty($_POST['colunas']) ? $_POST['colunas'] : null;
$colunasPesquisa = $_POST['colunasPesquisa'];
$dashboard = $_POST['dashboard'];
$inputidden = $_POST['inputidden'];
$checkbyname = !empty($_POST['checkbyname']) ? $_POST['checkbyname'] : null;
$get = array();
$restricted = !empty($_POST['restricted']) ? true : false;

$controller = ucfirst($tabela);
$view = $tabela;
$prefixo = substr($tabela, 0, 3);
$model = ucfirst($tabela);

$colunas = array();
$colunas_array = array();
$stmt = $pdo->prepare("SHOW COLUMNS FROM $tabela ");
if ($stmt->execute()) {
    $row = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    foreach ($row as $value) {
        array_push($colunas, $value['Field']);
        array_push($colunas_array, $value);
    }
}

$conteudoInterface = "export interface I" . $controller . "{\n";

foreach ($colunas_array as $c) {
    $type = explode("(", $c['Type']);
    switch (current($type)) {
        case 'bigint':
        case 'int':
            $conteudoInterface .= $c['Field'] . "?: number;\n";
            break;
        case 'varchar':
        case 'text':
            $conteudoInterface .= $c['Field'] . "?: string;\n";
            break;
        case 'decimal':
        case 'timestamp':
        case 'datetime':
        case 'date':
            $conteudoInterface .= $c['Field'] . "?: any;\n";
            break;
        default:
            $conteudoInterface .= $c['Field'] . "?: any;\n";
            break;
    }
}

$conteudoInterface .= "}";


$formIndexFilter = '<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
    <h2>' . ucfirst($controller) . '</h2>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<?php echo DC_TENANT; ?>/' . $dashboard . '">Home</a>
        </li>
        <li class="breadcrumb-item">
            <a>' . ucfirst($controller) . '</a>
        </li>
        <?php //if ($this->getPermissao(0, "criar")) { ?>
        <li class="breadcrumb-item">
            <a href="<?php echo DC_TENANT; ?>/' . $tabela . '/form">Novo</a>
        </li>
        <?php //} ?>
    </ol>
</div>
<div class="col-lg-2"></div>
</div>
<div class="wrapper wrapper-content animated fadeIn">
';

/*
 * Controller
 */
$conteudoController = '<?php
namespace App\controllers;
use Framework\core\Controller;
use App\models\\' . $controller . 'Model;';

if (!empty($selects)) {
    $select =  explode(",", $selects);
    foreach ($select as $s) {
        $conteudoController .= 'use App\models\\' . ucfirst($s) . 'Model;';
    }
}

$conteudoController .= 'class ' . $controller . 'Controller extends Controller {';

$conteudoController .= '
    public function __construct() {
        parent::__construct();
        $this->view->setActive("' . $view . '");';

$conteudoController .= $restricted ? '$this->restricted();' : '';
$conteudoController .= '}';

$conteudoController .= 'public function index() {
        $this->view->setTheme("' . $theme . '");';

$conteudoController .= ' $' . $prefixo . ' = new ' . $controller . 'Model;';

if (!empty($filtros) or !empty($selects)) {
    $formIndexFilter .= '
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Filtros</h5>
                </div>
                <div class="ibox-content">
                    <form role="form" method="get" action="<?php echo DC_TENANT; ?>/' . $controller . '" autocomplete="off">
                        <div class="row">';
}

if (!empty($filtros)) {
    $filters = explode(";", $filtros);
    foreach ($filters as $f) {
        $filtro = explode(",", $f);
        switch ($filtro[2]) {
            case 'text':
                $conteudoController .= 'if (!empty($_GET["' . $filtro[1] . '"])) {
                    $nome = str_replace(" ", "%", $_GET["' . $filtro[1] . '"]);
                    if (!strlen($nome) < 3) {
                        $' . $prefixo . '->set' . ucfirst($filtro[1]) . '($nome);
                    }
                    $this->view->setRegistros($_GET["' . $filtro[1] . '"], "pesq_' . $filtro[1] . '");
                }';
                $formIndexFilter .= '<div class="col-md-4 form-group">
                                <label>' . $filtro[1] . '</label>
                                <input type="text" name="' . $filtro[1] . '" class="form-control" value="<?php echo !empty($this->pesq_' . $filtro[1] . ') ? $this->pesq_' . $filtro[1] . ' : "" ?>">
                              </div>';
                array_push($get, $filtro[1]);
                break;
            case 'date':
                $conteudoController .= 'if (!empty($_GET["' . $filtro[1] . '"])) {
                    $init = str_replace("/", "-", $_GET["' . $filtro[1] . '"]);
                    $data_init = date("Y-m-d", strtotime($init));
                    $' . $prefixo . '->set' . ucfirst($filtro[1]) . '($data_init);
                    $this->view->setRegistros($_GET["' . $filtro[1] . '"], "pesq_' . $filtro[1] . '");
                }';
                $formIndexFilter .= '<div class="col-md-4 form-group" id="data_1">
                    <label>' . $filtro[1] . '</label>
                    <div class="input-group date">
                        <input type="text" class="form-control" name="' . $filtro[1] . '" 
                            value="<?php echo!empty($this->pesq_' . $filtro[1] . ') ? date("d/m/Y", strtotime($this->pesq_' . $filtro[1] . ')) : "" ?>">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>';
                array_push($get, $filtro[1]);
                break;
        }
    }
}

if (!empty($selects)) {
    $select =  explode(",", $selects);
    foreach ($select as $s) {
        $modelo = str_replace("id_", "", $s);
        $pref = randString(3);
        $idName = 'id_' . $s;
        $conteudoController .= '$' . $pref . ' = new ' . ucfirst($modelo) . 'Model;
        $' . $modelo . 's = $' . $pref . '->get' . ucfirst($modelo) . 's();
        $this->view->setRegistros($' . $modelo . 's, "' . $modelo . 's");';

        $formIndexFilter .= '<div class="col-md-3 form-group">
                <label>' . ucfirst($modelo) . '</label>
                <select class="form-control ctrl_select" name="' . $idName . '">
                    <option></option>
                    <?php if (!empty($this->' . $modelo . 's)) {
                        foreach ($this->' . $modelo . 's as $e) { ?>
                            <option value="<?php echo $e["' . $idName . '"] ?>" <?php echo !empty($this->pesq_' . $idName . ') && $this->pesq_' . $idName . ' == $e["' . $idName . '"] ? "selected" : ""; ?>>
                                <?php echo $e["no_' . $s . '"]; ?>
                            </option>
                    <?php } } ?>
                </select>
            </div>';
    }
}

$formIndexFilter .= '</div>';
$conteudoController .= '$this->view->setCSS(array("' . $view . '/index"));
        $this->view->setJS(array("index"));
        $this->view->render("index");
    }';

$conteudoController .= 'function form($id = 0) {
        $this->view->setTheme("' . $theme . '");';

foreach ($colunas_array as $c) {
    $type = explode("(", $c['Type']);
    
    // Ignorar created_at, updated_at, id_cadastrado_por
    if ($c['Field'] == 'created_at' || $c['Field'] == 'updated_at' || $c['Field'] == 'id_cadastrado_por') {
        continue;
    }

    switch (current($type)) {
        case 'bigint':
            $modelo = str_replace("id_", "", $c['Field']);
            $pref = randString(3);
            $conteudoController .= '$' . $pref . ' = new ' . ucfirst($modelo) . 'Model;
                    $' . $modelo . 's = $' . $pref . '->get' . ucfirst($modelo) . 's();
                    $this->view->setRegistros($' . $modelo . 's, "' . $modelo . 's");';
            break;
    }
}

$conteudoController .= 'if ($id) {
    $' . $prefixo . ' = new ' . $controller . 'Model;
    $' . $prefixo . '->set' . ucfirst($colunas[0]) . '($id);
    $' . $view . ' = $' . $prefixo . '->get' . $controller . '();
    $this->view->setRegistros($' . $view . ', "' . $view . '");
}
$this->view->setCSS(array("' . $view . '/form"));
$this->view->setJS(array("form"));
$this->view->render("form");
}';

$conteudoController .= 'function set() {
    $' . $prefixo . ' = new ' . $controller . 'Model;';
foreach ($colunas_array as $c) {
    if ($c['Field'] == 'created_at' || $c['Field'] == 'updated_at') {
        continue;
    }

    if ($c['Field'] == 'id_cadastrado_por') {
        $conteudoController .= '$' . $prefixo . '->set' . ucfirst($c['Field']) . '($this->pessoa());';
        continue;
    }

    $type = explode("(", $c['Type']);
    switch (current($type)) {
        case 'bigint':
            $conteudoController .= '$' . $prefixo . '->set' . ucfirst($c['Field']) . '($this->getPostInt("' . $c['Field'] . '"));';
            break;
        case 'int':
            $conteudoController .= '$' . $prefixo . '->set' . ucfirst($c['Field']) . '($this->getPostInt("' . $c['Field'] . '"));';
            break;
        case 'varchar':
            $conteudoController .= '$' . $prefixo . '->set' . ucfirst($c['Field']) . '($this->getPostString("' . $c['Field'] . '"));';
            break;
        case 'decimal':
            $conteudoController .= '$valor = str_replace(",", ".", $this->getPostParam("' . $c['Field'] . '"));
            $' . $prefixo . '->set' . ucfirst($c['Field']) . '($valor);';
            break;
        case 'date':
            $conteudoController .= '$init = str_replace("/", "-", $this->getPostParam("' . $c['Field'] . '"));
            $data_init = date("Y-m-d", strtotime($init));
            $' . $prefixo . '->set' . ucfirst($c['Field']) . '($data_init);';
            break;
    }
}

$conteudoController .= '$id = $' . $prefixo . '->set();
    if ($id) {
        echo json_encode(array("success" => true, "message" => "Dados cadastrados com sucesso!", "id" => $id));
        exit;
    } else {
        echo json_encode(array("error" => true, "message" => "Ocorreu um erro, por favor tente novamente!"));
        exit;
    }
}';

$conteudoController .= 'function del($id) {
    if ($this->filterInt($id)) {
        $' . $prefixo . ' = new ' . $controller . 'Model;
        $' . $prefixo . '->set' . ucfirst($colunas[0]) . '($id);
        if ($' . $prefixo . '->delete()) {
            echo json_encode(array("success" => true, "message" => "O cadastro foi excluído com sucesso!"));
        }
    }
}
}';

/*
 * Model
 */

$conteudoModel = '<?php
namespace App\models;
use Framework\core\Model;
class ' . $model . 'Model extends Model {';

$conteudoModel .= 'private ';
foreach ($colunas as $c) {
    if ($c == 'created_at' || $c == 'updated_at') {
        continue;
    }
    $conteudoModel .= '$' . $c;
    if ($c != end($colunas)) {
        $conteudoModel .= ',';
    }
}
$conteudoModel .= ';';

$conteudoModel .= 'function __construct() {
    parent::__construct();
}';

$conteudoModel .= 'function get' . $model . 's() {
    $sql = "SELECT * FROM ' . $tabela . ' ORDER BY ' . $colunas[0] . ' ASC";
    $res = $this->query($sql);
    return $res->fetchAll();
}';

$conteudoModel .= 'function get' . $model . '() {
    $sql = "SELECT * FROM ' . $tabela . ' WHERE ' . $colunas[0] . ' = ?";
    $params = array($this->get' . ucfirst($colunas[0]) . '());
    $res = $this->query($sql, $params);
    return $res->fetch();
}';

$conteudoModel .= 'function set() {
    if ($this->get' . ucfirst($colunas[0]) . '()) {
        $sql = "UPDATE ' . $tabela . ' SET ';
foreach ($colunas as $c) {
    if ($c == 'created_at' || $c == 'updated_at') {
        continue;
    }
    $conteudoModel .= $c . ' = ?';
    if ($c != end($colunas)) {
        $conteudoModel .= ',';
    }
}
$conteudoModel .= ' WHERE ' . $colunas[0] . ' = ?;";
$params = array(';
foreach ($colunas as $c) {
    if ($c == 'created_at' || $c == 'updated_at') {
        continue;
    }
    $conteudoModel .= '$this->get' . ucfirst($c) . '(),';
}
$conteudoModel .= '$this->get' . ucfirst($colunas[0]) . '());
$res = $this->query($sql, $params);
$res->rowCount();
return $this->get' . ucfirst($colunas[0]) . '();
} else {
    $sql = "INSERT INTO ' . $tabela . ' (';
foreach ($colunas as $c) {
    if ($c == 'created_at' || $c == 'updated_at') {
        continue;
    }
    $conteudoModel .= $c;
    if ($c != end($colunas)) {
        $conteudoModel .= ',';
    }
}
$conteudoModel .= ') VALUES (';
foreach ($colunas as $c) {
    if ($c == 'created_at' || $c == 'updated_at') {
        continue;
    }
    $conteudoModel .= '?';
    if ($c != end($colunas)) {
        $conteudoModel .= ',';
    }
}
$conteudoModel .= ');";
$params = array(';
foreach ($colunas as $c) {
    if ($c == 'created_at' || $c == 'updated_at') {
        continue;
    }
    $conteudoModel .= '$this->get' . ucfirst($c) . '()';
    if ($c != end($colunas)) {
        $conteudoModel .= ',';
    }
}
$conteudoModel .= ');
$this->query($sql, $params);
return $this->lastInsertId();
}
}';

// Adicionar os getters e setters para os campos restantes
foreach ($colunas as $c) {
    if ($c == 'created_at' || $c == 'updated_at') {
        continue;
    }
    $conteudoModel .= 'function get' . ucfirst($c) . '() {
        return $this->' . $c . ';
    }';

    $conteudoModel .= 'function set' . ucfirst($c) . '($' . $c . ') {
        $this->' . $c . ' = $' . $c . ';
    }';
}

$conteudoModel .= '}';

/*
 * Output the content
 */

$conteudo = "";
$arquivo_name = "Response.php";
if (!empty($_POST['model'])) {
    $conteudo .= $conteudoModel;
    $arquivo_name = $model . "Model.php";
}
if (!empty($_POST['controller'])) {
    $conteudo .= $conteudoController;
    $arquivo_name = $controller . "Controller.php";
}
if (!empty($_POST['index'])) {
    $conteudo .= $formIndexFilter;
    $arquivo_name = "index.php";
}
if (!empty($_POST['form'])) {
    $conteudo .= $conteudoForm;
    $arquivo_name = "form.php";
}
if (!empty($_POST['interface'])) {
    $conteudo = $conteudoInterface;
    $arquivo_name = $tabela.".interface.ts";
}


if (!empty($conteudo)) {
    $arquivo = fopen($arquivo_name, 'w');
    fwrite($arquivo, $conteudo);
    fclose($arquivo);

    $file = $arquivo_name;

    header("Content-Length: " . filesize($file));
    header("Content-Disposition: attachment; filename=" . basename($file));
    readfile($file); // lê o file
    unlink("./$arquivo_name");
    exit;
} else {
    echo "Escolha ao menos uma opção";
    exit;
}

function randString($size)
{
    $basic = 'abcdefghijklmnopqrstuvwxyz';
    $return = "";
    for ($count = 0; $size > $count; $count++) {
        $return .= $basic[rand(0, strlen($basic) - 1)];
    }
    return $return;
}
